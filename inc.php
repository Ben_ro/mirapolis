﻿<?php
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

include 'config.php';

require 'mail/src/Exception.php';
require 'mail/src/PHPMailer.php';
require 'mail/src/SMTP.php';
require 'mysql/vendor/autoload.php';
	
ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

function signit($url, $params) {// url REST-запроса и массив параметров
global $appid, $secretkey;
 $ret_params = $params;//массив передаваемых параметров
 ksort($ret_params); //сортировка параметров по названию
 $ret_params['appid'] = $appid; //помещение в конец массива параметра appid

 $signstring="$url?";//формирование строки для подписи начиная с url
 foreach ($ret_params as $key => $val) {
 if (($val!="")||(gettype($val)!="string")) {
 $signstring .= "$key=$val&";//добавление в строку для подписи очередного параметра
 }
 }
 $signstring .= "secretkey=$secretkey";//дополнение строки для подписи параметром secretkey
 $ret_params['sign'] = strtoupper(md5($signstring)); //формирование ключа и добавление его в
 // массив параметров
 return $ret_params;
}

function sendrequest($url, $parameters, $method, $ret_crange) {
 //дополнение массива параметров значениями appid и sign (используется выше описанная функция signit)
 $curl_data = signit($url, $parameters);
 $ch = curl_init();//инициализация дескриптора запроса
 curl_setopt($ch, CURLOPT_ENCODING, 'UTF-8'); //задание кодировки запроса
 curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); //возврат результата
 curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1); //делает возможным переход на страницу ошибки
 curl_setopt($ch, CURLOPT_HEADER, $ret_crange); //делает возможным возвращение заголовка ContentRange
 curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method); //задание метода запроса
 $query = http_build_query($curl_data); //построение строки параметров
 switch ($method) {
case "PUT"://для PUT необходимо передавать длину строки параметров
curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Length: " . strlen($query)));
case "POST"://параметры PUT и POST передаются в теле запроса
curl_setopt($ch, CURLOPT_POSTFIELDS, $query);
break;
case "GET"://для GET и DELETE параметры указываются в заголовке
case "DELETE":
$url .= "?$query";
 }
 curl_setopt($ch, CURLOPT_URL, $url); //задание url запроса
 

 $curl_response = curl_exec($ch); //выполнение запроса
 // print_r($curl_response);
 $response = json_decode($curl_response, true); //парсинг результатов
 if (!$response) $response = $curl_response; //если результат не json
 $code = curl_getinfo($ch, CURLINFO_HTTP_CODE); //получение кода результата
 curl_close($ch);
 //анализ ответа
 switch ($code){
	 case 200:
		return $response;
		break;
	case 201:
		return $response;
		break;
	case 304:
		return $response;
		break;
	case 400:
		throw new Exception("Некорректный запрос: ".$code);
		break;
	case 401:
		throw new Exception("Неавторизированный доступ: ".$code);
		break;
	case 404:
		throw new Exception("Данные не найдены: ".$code);
		break;
	case 403:
		throw new Exception("Доступ запрещен: ".$code);
		break;
	case 500:
		throw new Exception("Внутренняя ошибка сервера: ".$code);
		break;
 }
 /*if ($code != 200) {
 throw new Exception("Неправильный HTTP-код: ".$code);
 } else if (is_array($response)&&isset($response["errorMessage"])) {
 throw new Exception("Возвращена ошибка: ".$response["errorMessage"]);
 } else {
 return $response;
 }*/
}
function get_webinars(){
	$request = mysql("SELECT * FROM webinars WHERE active = 1");
	if($request->num_rows !== 0){
		while ($actor = $request->fetch_assoc()) {
			$array[] = $actor;
		}
		return $array;
	}else{
		return '';
	}
	
}
function get_prepod(){
	$request = mysql("SELECT * FROM teacher");
	if($request->num_rows !== 0){
		while ($actor = $request->fetch_assoc()) {
			$array[$actor['userid']] = $actor;
		}
		return $array;
	}else{
		return '';
	}
	/*foreach ($array as $arr){
		$teacher[$arr['userid']] = $arr;
	}
	return $teacher;*/
}
function disable_webinars($array){
	foreach($array as $key => $arr){
		$request = mysql("UPDATE webinars SET active = 0 WHERE id = {$key}");
		$view = json_encode($arr, JSON_UNESCAPED_UNICODE);
		$request = mysql("UPDATE webinars SET view = '{$view}' WHERE id = {$key}");
	}
	
}
function add_webinar($array, $day, $teach, $number){
	//mysql('TRUNCATE TABLE webinars_week');
	//$request = mysql("INSERT INTO webinars_week (webid, name, sdate, edate, url) VALUES ('{$array['id']}', '{$array['name']}', '{$array['sdate']}', '{$array['edate']}', 'url')");
	$request = mysql("INSERT INTO webinars (webid, name, day, number,teacher, sdate, edate, active, url) VALUES ('{$array['id']}', '{$array['name']}', '{$day}', '{$number}','{$teach}', '{$array['sdate']}', '{$array['edate']}', '1', '{$array['link']}')");

}
function add_user($id, $fio, $mail){
	//mysql('TRUNCATE TABLE webinars_week');
	//$request = mysql("INSERT INTO webinars_week (webid, name, sdate, edate, url) VALUES ('{$array['id']}', '{$array['name']}', '{$array['sdate']}', '{$array['edate']}', 'url')");
	$request = mysql("INSERT INTO teacher (userid, fio, email) VALUES ('{$id}', '{$fio}', \"{$mail}\")");

}
function create_fold($date){
	global $url_general;
	$params = [
		"gcname" => $date,
		"gcparentid" => 12,
		"viewaccess" => 1,
		"manageaccess" => 1
		//"gcparentid" => 35
	];
	$service_url = $url_general."/measureGroups";
	$res = sendrequest($service_url, $params, "POST", 0);
	return $res;
}
function create_user ($fio, $email, $group = 6){
	global $url_general;
	$password = gen_pass();
	if(isset(explode(' ', $fio)[1])){
		$name = explode(' ', $fio)[1];
	}else{
		$name = '';
	}
	if(isset(explode(' ', $fio)[0])){
		$fa = explode(' ', $fio)[0];
	}else{
		$fa = '';
	}
	if(isset(explode(' ', $fio)[2])){
		$ot = explode(' ', $fio)[2];
	}else{
		$ot = '';
	}
	
	
	$username = mb_strtolower(translit($name)[0].'_'.translit($fa));
	
	$service_url = $url_general."/persons";
	$params = [
		"plastname" => $fa,
		"pfirstname" => $name,
		"psurname" => $ot,
		"isuser" => 1,
		"pilogin" => $username,
		"pipassword" => $password,
		"personemail" => $email,
	];
	//print_r($params);
	//die;
	$res = sendrequest($service_url, $params, "POST", 0);
	$user_id = $res;
	$service_url = $url_general."/personGroups/$group/childPersons/$user_id";
	$res = sendrequest($service_url, array(), "POST", 0);
	$service_url = $url_general."/roles/9/addToPerson/$user_id";
	$res = sendrequest($service_url, array(), "POST", 0);
	if($user_id){
		add_user($user_id, $fio, $email);
		echo $fio.' - '.$username.' : '.$password.'<br>';
		$html = '
		<div class="html-parser" style="border-color: rgb(31, 31, 31) !important;">
			<div class="html-fishing" style="border-color: rgb(31, 31, 31) !important;">
				<div class="html-expander" style="border-color: rgb(31, 31, 31) !important;">
					<div class="js-helper js-readmsg-msg" style="border-color: rgb(31, 31, 31) !important;">
						<style type="text/css" style="border-color: rgb(31, 31, 31) !important;"></style>
						<div style="border-color: rgb(31, 31, 31) !important;">
							<div style="border-color: rgb(31, 31, 31) !important;"><div class="class_1584411227" style="border-color: rgb(31, 31, 31) !important;">
							<style style="border-color: rgb(31, 31, 31) !important;">
								.class_1584411227 a{
									text-decoration:none;
								}
								.class_1584411227 body{
									background-color:#ffffff;
									background-image:none;
									background-position:left top;
									background-repeat:repeat;
									margin-top:0px;
									margin-right:0px;
									margin-bottom:0px;
									margin-left:0px;
									padding-top:0px;
									padding-right:0px;
									padding-bottom:0px;
									padding-left:0px;
								}
								@media  only screen and (max-width: 480px){
									.class_1584411227 .mobileHidden_mailru_css_attribute_postfix{
										display:none !important;
									}
									.class_1584411227 .mobileContentPadding_mailru_css_attribute_postfix{
										width:24px !important;
									}
								}
							</style>
							<table align="center" width="100%" cellpadding="0" cellspacing="0" border="0" bgcolor="#212121" style="max-width: 640px; background-color: rgb(33, 33, 33) !important;">
								<tbody style="border-color: rgb(255, 255, 255) !important;"><tr style="border-color: rgb(31, 31, 31) !important;">
									<td height="20" style="border-color: rgb(31, 31, 31) !important;">&nbsp;</td>
									<td height="20" style="border-color: rgb(31, 31, 31) !important;">&nbsp;</td>
									<td height="20" style="border-color: rgb(31, 31, 31) !important;">&nbsp;</td>
								</tr>
								<tr style="border-color: rgb(31, 31, 31) !important;">
									<td width="35" style="border-color: rgb(31, 31, 31) !important;">&nbsp;</td>
									<td align="center" valign="top" width="570" style="border-color: rgb(31, 31, 31) !important;">
										<table name="mailruanchor_logo" align="center" width="100%" cellpadding="0" cellspacing="0" border="0" style="border-color: rgb(255, 255, 255) !important;">
											<tbody style="border-color: rgb(31, 31, 31) !important;"><tr>
												<td style="border-color: rgb(31, 31, 31) !important;">
													<img align="left" width="100" height="21" alt="МИИГАиК" src="https://swh.miigaik.ru/phpmailer/logo.png" border="0" style="border-color: rgb(31, 31, 31) !important;">
												</td>
											</tr>
											<tr>
												<td height="15" style="border-color: rgb(31, 31, 31) !important;">&nbsp;</td>
											</tr>
										</tbody></table>
										<table name="mailruanchor_content" width="100%" cellpadding="0" cellspacing="0" border="0" bgcolor="#1F1F1F" style="font-size: 16px; box-shadow: rgba(0, 0, 0, 0.1) 0px 1px 2px 0px; background-color: rgb(31, 31, 31) !important;color: #D1D1D1;">
											<tbody style="border-color: rgb(255, 255, 255) !important;"><tr style="border-color: rgb(31, 31, 31) !important;">
												<td class="mobileContentPadding_mailru_css_attribute_postfix" width="48" style="border-color: rgb(31, 31, 31) !important;">&nbsp;</td>
												<td style="border-color: rgb(31, 31, 31) !important;">
													<table width="100%" cellpadding="0" cellspacing="0" border="0" style="border-color: rgb(255, 255, 255) !important;">
														<tbody style="border-color: rgb(31, 31, 31) !important;"><tr>
															<td width="100%" height="52" style="border-color: rgb(31, 31, 31) !important;">&nbsp;</td>
														</tr>
														<tr>
															<td style="font-size: 38px; letter-spacing: 0px; line-height: 44px; font-weight: 600; border-color: rgb(31, 31, 31) !important;">
																Учетная запись преподавателя была создана
															</td>
														</tr>
														<tr>
															<td width="100%" height="28" style="border-color: rgb(31, 31, 31) !important;">&nbsp;</td>
														</tr>
														<tr>
															<td style="border-color: rgb(31, 31, 31) !important;">
																<table width="100%" cellpadding="0" cellspacing="0" border="0" style="border-color: rgb(255, 255, 255) !important;">
																		<tbody style="border-color: rgb(31, 31, 31) !important;"><tr>
																			<td style="border-color: rgb(31, 31, 31) !important;">
																				Данная учетная запись используется для доступа к Mirapolis Virtual Room МИИГАиК<br>
																				Имя пользователя: <span style="font-weight: 600; border-color: rgb(31, 31, 31) !important;">'.$username.'</span><br style="border-color: rgb(31, 31, 31) !important;">
																				Временный пароль: <span style="font-weight: 600; border-color: rgb(31, 31, 31) !important;">'.$password.'</span>
																			</td>
																		</tr>
																		<tr>
																			<td width="100%" height="20" style="border-color: rgb(31, 31, 31) !important;">&nbsp;</td>
																		</tr>
																</tbody></table>
															</td>
														</tr>
														<tr>
															<td width="100%" height="28" style="border-color: rgb(31, 31, 31) !important;">&nbsp;</td>
														</tr>
													
														<tr>
															<td style="border-color: rgb(31, 31, 31) !important;">
																<table cellspacing="0" cellpadding="0" border="0" style="max-width: 100%; border-color: rgb(255, 255, 255) !important;">
																	<tbody style="border-color: rgb(31, 31, 31) !important;"><tr style="cursor: pointer;">
																		<td bgcolor="#D93C02" style="border-color: rgb(216, 59, 1); min-width: 140px; text-align: center; padding: 10px; border-radius: 2px; background-color: rgb(217, 60, 2) !important;">
																				<a style="color: rgb(255, 255, 255); text-decoration: none; display: block; border-color: rgb(31, 31, 31) !important;" href="http://b45187.vr.mirapolis.ru/mira" target="_blank" rel=" noopener noreferrer">
																					<strong style="text-decoration: none; color: rgb(255, 255, 255); font-weight: normal; border-color: rgb(31, 31, 31) !important;">Вход в Mirapolis Virtual Room</strong>
																				</a>
																		</td>
																	</tr>
																</tbody></table>
															</td>
														</tr>
														<tr>
															<td width="100%" height="72" style="border-color: rgb(31, 31, 31) !important;">&nbsp;</td>
														</tr>
													</tbody></table>
												</td>
												<td class="mobileContentPadding_mailru_css_attribute_postfix" width="48" style="border-color: rgb(31, 31, 31) !important;">&nbsp;</td>
											</tr>
										</tbody></table>
										<table name="mailruanchor_footer" align="center" width="100%" cellpadding="0" cellspacing="0" border="0" style="font-size: 11px; color: rgb(209, 209, 209) !important; letter-spacing: 0px; line-height: 13px; border-color: rgb(255, 255, 255) !important;">
											<tbody style="border-color: rgb(31, 31, 31) !important;"><tr>
												<td width="100%" height="24" style="border-color: rgb(31, 31, 31) !important;">&nbsp;</td>
											</tr>
											<tr>
												<td width="100%" height="15" style="border-color: rgb(31, 31, 31) !important;">&nbsp;</td>
											</tr>
											<tr>
												<td style="border-color: rgb(31, 31, 31) !important;">
													Московский государственный университет геодезии и картографии, Гороховский пер., 4, Москва, 105064
												</td>
											</tr>
											<tr>
												<td width="100%" height="15" style="border-color: rgb(31, 31, 31) !important;">&nbsp;</td>
											</tr>
											<tr>
												<td style="border-color: rgb(31, 31, 31) !important;">
													<img width="100" height="21" alt="МИИГАиК" src="https://swh.miigaik.ru/phpmailer/logo.png" border="0" style="border-color: rgb(31, 31, 31) !important;">
												</td>
											</tr>
											<tr>
												<td width="100%" height="36" style="border-color: rgb(31, 31, 31) !important;">&nbsp;</td>
											</tr>
										</tbody></table>
									</td>
									<td width="35" style="border-color: rgb(31, 31, 31) !important;">&nbsp;</td>
								</tr>
							</tbody></table>
							
						</div>
						</div>	
					</div>
					</div>
				</div>
			</div>
		</div>
		';
		send_mail($email, $email, 'Регистрация МИИГАиК Mirapolis Virtual Room', $html, 0);
	}else{
		echo "$fio - что то пошло не так";
	}
}
function create_web ($mname, $sdate, $edate, $tutors, $child = 0){
	global $url_general;
	$token = login();
	$service_url = $url_general."/measures";
	//задание массива параметров мероприятия
	$parameters = array(
	 'mename' => $mname,
	 'mestartdate' => $sdate,
	 'meenddate' => $edate,
	 'usrsesid' => $token,
	 'metype' => '1',
	 'meeduform' => '0'
	);
	$res = sendrequest($service_url, $parameters, "POST", 0);
	$array['id'] = $res['meid'];
	$array['name'] = $res['mename'];
	$array['sdate'] = $res['mestartdate'];
	$array['edate'] = $res['meenddate'];
	$meid = $res['meid'];
	$service_url = $url_general."/measures/$meid/tutors/4140";
	$res = sendrequest($service_url, array('usrsesid' => $token), "DELETE", 0);
	$service_url = $url_general."/measures/$meid/tutors/$tutors";
	$res = sendrequest($service_url, array('usrsesid' => $token), "POST", 0);
	if($child == 0){
		$service_url = $url_general."/measureGroups/10/childMeasures/$meid";
		
	}else{
		$service_url = $url_general."/measureGroups/$child/childMeasures/$meid";
	}
	$res = sendrequest($service_url, array('usrsesid' => $token), "POST", 0);
	$service_url = $url_general."/measures/$meid/webinarAnonymousLink";
	$res = sendrequest($service_url, array('usrsesid' => $token), "GET", 0);
	$array['link'] = $res;
	logout($token);
	//print_r($res);
	return $array;
}
function login(){
	global $url_general;
	$service_url = $url_general."/auth/login";
	$parameters = array(
	'login' => 'n_los',
	'password' => 'Los310196'
	);
	$res = sendrequest($service_url, $parameters, "POST", 0);
	return $res['access_token'];
}
function logout($token){
	global $url_general;
	$service_url = $url_general."/auth/logout";
	$res = sendrequest($service_url, array('usrsesid' => $token), "POST", 0);
}
function get_record ($mid){
	global $url_general;
	$service_url = $url_general."/measures/$mid/webinarRecords";
	$res = sendrequest($service_url, array(), "GET", 0);
	return $res;
}
function send_mail($mailTo, $nameTo, $subject, $html, $bcc = 0){
	global $smtp;
	$mail = new PHPMailer;
	$mail->isSMTP(); 
	$mail->SMTPDebug = 0;
	$mail->Host = $smtp->host;
	$mail->Port = $smtp->port;
	$mail->SMTPSecure = $smtp->type;
	$mail->SMTPAuth = true;
	$mail->SMTPOptions = array(
		'ssl' => array(
			'verify_peer' => false,
			'verify_peer_name' => false,
			'allow_self_signed' => true
		)
	);
	$mail->CharSet = 'UTF-8';
	$mail->Username = $smtp->user;
	$mail->Password = $smtp->pass;
	$mail->msgHTML($html); 
	$mail->setFrom($smtp->user, $smtp->name);
	$mail->addAddress($mailTo, $nameTo);
	if($bcc != 0){
		$mail->AddBCC('lit@miigaik.ru', 'portal@miigaik.ru');
	}
	$mail->Subject = $subject;
	$mail->AltBody = 'HTML messaging not supported';

	if(!$mail->send()){
		echo "Mailer Error: " . $mail->ErrorInfo;
	}else{
		//echo "Message sent!";
	}
}
function gen_pass(){
$chars = "qazxswedcvfrtgbnhyujmkiolp1234567890QAZXSWEDCVFRTGBNHYUJMKIOLP";
$max = 8;
$size = StrLen($chars)-1;
$password = null;
	while($max--)
    $password.=$chars[rand(0,$size)];

return $password;
}
function translit($str){
    $tr = array(
        "А"=>"A","Б"=>"B","В"=>"V","Г"=>"G",
        "Д"=>"D","Е"=>"E","Ж"=>"J","З"=>"Z","И"=>"I",
        "Й"=>"Y","К"=>"K","Л"=>"L","М"=>"M","Н"=>"N",
        "О"=>"O","П"=>"P","Р"=>"R","С"=>"S","Т"=>"T",
        "У"=>"U","Ф"=>"F","Х"=>"H","Ц"=>"TS","Ч"=>"CH",
        "Ш"=>"SH","Щ"=>"SCH","Ъ"=>"","Ы"=>"YI","Ь"=>"",
        "Э"=>"E","Ю"=>"YU","Я"=>"YA","а"=>"a","б"=>"b",
        "в"=>"v","г"=>"g","д"=>"d","е"=>"e","ж"=>"j",
        "з"=>"z","и"=>"i","й"=>"y","к"=>"k","л"=>"l",
        "м"=>"m","н"=>"n","о"=>"o","п"=>"p","р"=>"r",
        "с"=>"s","т"=>"t","у"=>"u","ф"=>"f","х"=>"h",
        "ц"=>"ts","ч"=>"ch","ш"=>"sh","щ"=>"sch","ъ"=>"y",
        "ы"=>"yi","ь"=>"","э"=>"e","ю"=>"yu","я"=>"ya", "ё" => 'e',
    "."=>"_"," "=>"_","?"=>"_","/"=>"_","\\"=>"_",
    "*"=>"_",":"=>"_","*"=>"_","\""=>"_","<"=>"_",
    ">"=>"_","|"=>"_"
    );
    return strtr($str,$tr);
}
function mysql($sql){
$mysqli = new mysqli('10.1.1.90:3310', 'mirapolis', 'Nikita310196', 'mirapolis');
$mysqli->set_charset("utf8");
if ($mysqli->connect_errno) {
	echo "Извините, возникла проблема на сайте";
	die;
}else{
	if (!$result = $mysqli->query($sql)) {
		echo "2Извините, возникла проблема в работе сайта.<br>";
		echo "Ошибка: Наш запрос не удался и вот почему: <br>";
		echo "Запрос: " . $sql . "<br>";
		echo "Номер ошибки: " . $mysqli->errno . "<br>";
		echo "Ошибка: " . $mysqli->error . "<br>";
		die;
	}else{
		return $result;
	}
}
/*$link = mysqli_connect('10.1.1.90:3310', 'mirapolis', 'Nikita310196', 'mirapolis')
    or die("Ошибка " . mysqli_error($link));
mysql_set_charset("utf8")
 
$result = mysqli_query($link, $query) or die("Ошибка " . mysqli_error($link)); 

mysqli_close($link);
return $result;*/
}