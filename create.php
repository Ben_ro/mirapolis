<?php
include 'inc.php';

ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
die;
$webs = [
	['по информатике', '2019-12-02 17:00:00.000', '2019-12-02 18:30:00.000', '2462'],
	['по географии', '2019-12-02 19:00:00.000', '2019-12-02 20:30:00.000', '2465'],
	['по математике', '2019-12-03 17:00:00.000', '2019-12-03 18:30:00.000', '4158'],
	['по русскому языку', '2019-12-05 19:00:00.000', '2019-12-05 20:30:00.000', '4151']
];

$oldWeb = file_get_contents('/var/www/www-root/data/www/swh.miigaik.ru/mirapolis/results.json');
$oldWeb = json_decode($oldWeb);


foreach ($oldWeb as $old){
	$rec = get_record($old->id);
	if(is_array($rec)){
		foreach($rec as $record){
			$date = explode(" - ", $record['period']);
			$sdate = new DateTime($old->sdate);
			$sdate->sub(new DateInterval('PT00H30M'));
			$edate = new DateTime($old->edate);
			$edate->add(new DateInterval('P30M'));
			if($date[0] >= $sdate->format('Y-m-d H:i:s') and $date[0] < $edate->format('Y-m-d H:i:s') and $record['duration'] > '00:05:00'){
				$result[$record['name']][] = $record['viewLink'];
			}
		}
	}else{
		$result[$old->name][] = 'Нет записи';
	}
}

$html = '<b>Записи с прошлой недели</b><br><hr>';

foreach($result as $name => $value){
	$html .= $name.':<br>';
	foreach($value as $val){
		$html .= $val.'<br>';
	}
}

foreach($webs as $key => $web){
	$sdaten = new DateTime($oldWeb[$key]->sdate);
	$sdaten->add(new DateInterval('P07D'));
	$edaten = new DateTime($oldWeb[$key]->edate);
	$edaten->add(new DateInterval('P07D'));
	if(!empty($oldWeb[$key]->name)){
		$number = preg_replace('~[^0-9]+~','',$oldWeb[$key]->name)+1;
	}else{
		$number = '9';
	}
	$array[] = create_web("Вебинар № $number ".$web[0], $sdaten->format('Y-m-d H:i:s').'.000', $edaten->format('Y-m-d H:i:s').'.000', $web[3]); 
}

$html .= '<br><b>Ссылки для слушателей на эту неделю</b><br><hr>';

foreach ($array as $arr){
	$html .= $arr['name'].' - '.$arr['link'].'<br>';
}
echo $html;
$fp = fopen('/var/www/www-root/data/www/swh.miigaik.ru/mirapolis/results.json', 'w');
fwrite($fp, json_encode($array, JSON_UNESCAPED_UNICODE));
fclose($fp);
send_mail('teach330@gmail.com', 'teach330@gmail.com', 'Вебинары', $html, 1);