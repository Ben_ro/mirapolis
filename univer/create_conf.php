<?php
die;
include '../inc.php';

ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

$times = [
	1 => ['11:00:00', '14:00:00'],
	2 => ['11:00:00', '16:00:00'],
	3 => ['09:00:00', '18:00:00'],
	4 => ['09:00:00', '18:00:00'],
	5 => ['16:10:00', '17:40:00'],
	6 => ['17:50:00', '19:20:00'],
	7 => ['19:30:00', '21:00:00'],
];
$week = [
	1 => 'Monday',
	2 => 'Tuesday',
	3 => 'Wednesday',
	4 => 'Thursday',
	5 => 'Friday',
	6 => 'Saturday',
	7 => 'Sunday',
];
$webs = [
	/*1 => [
		['Открытие студенческой недели науки', '10384', 1],
		['Секция 9 «Топографическое и навигационное обеспечение безопасности и обороны»', '10723', 2],
		['Секция 14 «Геодезия, картография и кадастр»', '10269', 2],
	],*/
	2 => [
		['Секция 1 «Высшая геодезия, космическая геодезия и астрономия»', '10379', 3],
		['Секция 2 «Информационная безопасность» ', '10234', 3],
		['Секция 3 «Природопользование и мониторинг земель»', '10190', 3],
		['Секция 4 «Геодезия и топография»', '10279', 3],
	],
	3 => [
		['Секция 5 «Прикладная геодезия»', '10722', 4],
		['Секция 6 «Государственные кадастры и системы учета недвижимости»', '10264', 4],
		['Секция 7 «Картография»', '5386', 4],
		['Секция 8 «Архитектура и градостроительство»  ', '5400', 4],
	],
	4 => [
		['Секция 10 «Фотоника, приборостроение, оптические системы и технологии»', '6', 4],
		['Секция 11 «Устойчивое развитие территорий»', '10307', 4],
		['Секция 12 «Аэрокосмические исследования Земли и фотограмметрия»', '10226', 4],
		['Секция 13 «Геоинформационные системы и технологии»', '10238', 4],
	],
	5 => [
		['Закрытие студенческой недели науки. Видеофорум, посвященный 75-летию Победы в Великой Отечественной войне', '10723', 1],
	]
];
//$old_webinars = get_webinars();

if($old_webinars){
	foreach($old_webinars as $web){
		$rec = get_record($web['webid']);
		if(is_array($rec)){
			foreach($rec as $record){
				$date = explode(" - ", $record['period']);
				$sdate = new DateTime($web['sdate']);
				$sdate->sub(new DateInterval('PT00H30M'));
				$edate = new DateTime($web['edate']);
				$edate->add(new DateInterval('P30M'));
				if($date[0] >= $sdate->format('Y-m-d H:i:s') and $date[0] < $edate->format('Y-m-d H:i:s') and $record['duration'] > '00:05:00'){
					$result[$web['id']][] = $record['viewLink'];
				}
			}
			if(empty($result[$web['id']])){
				$result[$web['id']][] = 'Нет записи';
			}
		}else{
			$result[$web['id']][] = 'Нет записи';
		}
	}
	disable_webinars($result);
}
foreach  ($webs as $key => $weba){
	$a = new DateTime();
	$a->modify('next '.$week[$key]);
	$day = $a->format('Y-m-d');
//	if($key == 3){
//		$day = '2020-03-25';
//	}			

	//die;
	//$fold = create_fold($day);
	foreach($weba as $web){
		$sdate = $day.' '.$times[$web[2]][0].'.000';
		$edate = $day.' '.$times[$web[2]][1].'.000';
		$name = $web[0];
		$webinar = create_web($name, $sdate, $edate, $web[1], 35); //заменить fold на id (ID = 35) 
	//	add_webinar($webinar, $key, $web[1], $web[2]);
	}
}